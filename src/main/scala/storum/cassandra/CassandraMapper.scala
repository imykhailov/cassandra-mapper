package storum.cassandra

import com.datastax.driver.core.Row
import scala.collection.JavaConverters._
import org.joda.time.DateTime
import java.util.UUID
import com.datastax.driver.core.Session
import com.datastax.driver.core.querybuilder.Insert
import java.net.InetAddress
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.spatial4j.core.context.SpatialContext
import com.spatial4j.core.shape.Point
import com.datastax.driver.core.RegularStatement
import java.nio.ByteBuffer
import com.datastax.driver.core._
import storum.cassandra.removableoption._


/**
 * Cassandra row to case objects mapping, inspired by Anorm 
 * 
 * Include it into your DAO as import storum.cassandra.CassandraMapper._
 * 
 * Create collection mapping as:
 * val mapping = get[UUID]("id") ~
 *               getOptional[Int]("price") ~
 *               get[String]("name")
 * 
 * Create parser:
 * val parser = mapping parser {
 *    case
 *      itemId ~
 *      price ~
 *      name =>
 *        
 *      Item(
 *        itemId = itemId,
 *        price = price, 
 *        name = name, 
 *      )
 *  }
 * 
 * and saver:
 * val saver = itemDescription saver { (i: Item) =>
 *    i.itemId ~
 *    i.price ~
 *    i.name
 * }
 * 
 * Use it in dao methods:
 * def listByIds(itemIds: Seq[UUID]): Seq[Item] = {
 *    val query = QueryBuilder
 *      .select(mapping.columnNames: _*)
 *      .from("items")
 *      .where(QueryBuilder.in(wrapInQuotes("itemId"), itemIds: _*))
 *    
 *    execute(query).map(itemParser)
 *  }
 */
object CassandraMapper extends Formats {

  /**
   * Wrap string in "" if it is not wrapped. Use for wrap cassandra columns
   * and tables names to support case sensitiveness
   */
  def wrapInQuotes(identif: String) = {
    val tmp = if (identif(0) != '"') '"' + identif else identif
    if (tmp.last != '"') tmp + '"' else tmp
  }

  def i = wrapInQuotes _

  case class ~[+A, +B](_1: A, _2: B)

  implicit class TildaSupporter[A](val value: A) extends AnyRef {
    def ~[B](b: B) = new ~(value, b)
  }

  trait RowDescriptor[A] {
    def parse(row: Row): A

    def save(query: Insert, value: A): Insert

    def columnNames: List[String]

    def ~[B](p: RowDescriptor[B]): RowDescriptor[A ~ B] = {
      val a = this
      val b = p
      new RowDescriptor[A ~ B] {
        def parse(row: Row): A ~ B = {
          new ~(a.parse(row), b.parse(row))
        }

        def save(query: Insert, value: A ~ B) = {
          val tmpQuery = a.save(query, value._1)
          b.save(tmpQuery, value._2)
        }

        def columnNames = a.columnNames ::: b.columnNames
      }
    }

    def parser[EntityType](f: A => EntityType): Row => EntityType =
      (row: Row) => {
        try {
          f(this.parse(row))
        } catch {
          case e: Exception => 
            throw new CassandraMapperException(message = Some(e.getMessage()), cause = Some(e), rowParam = Some(row))
        }
      }

    def saver[EntityType](f: EntityType => A): (Insert, EntityType) => Insert =
      (query: Insert, entity: EntityType) =>
        try {
          this.save(query, f(entity))
        } catch {
          case e: Exception => 
            throw new CassandraMapperException(message = Some(e.getMessage()), cause = Some(e), queryParam = Some(query))
        }
  }

  /**
   * map one column with optional value
   */
  def getOptional[T](columnName: String)(implicit format: Format[T]): RowDescriptor[Option[T]] = {
    new RowDescriptor[Option[T]] {
      def parse(row: Row) = format.reads(row, i(columnName))

      def save(query: Insert, value: Option[T]) = format.writes(query, i(columnName), value)

      def columnNames = List(i(columnName))
    }
  }
  
  /**
   * map tri-state removable optional column
   */
  def getRemovableOptional[T](columnName: String)(implicit format: Format[T]): RowDescriptor[RemovableOption[T]] = {
    new RowDescriptor[RemovableOption[T]] {
      def parse(row: Row): RemovableOption[T] = format.reads(row, i(columnName))

      def save(query: Insert, value: RemovableOption[T]) = value match {
        case RNone => 
          query
        case RRemoved => 
          query.value(i(columnName), null)
        case RSome(v) =>
          format.writes(query, i(columnName), value.toOption)
      }

      def columnNames = List(i(columnName))
    }
  }

  /**
   * map column, use default if value is absent
   */
  def get[T](columnName: String, default: => T)(implicit format: Format[T]): RowDescriptor[T] = {
    new RowDescriptor[T] {
      def parse(row: Row) = format.reads(row, i(columnName)).getOrElse(default)

      def save(query: Insert, value: T) = format.writes(query, i(columnName), Some(value))

      def columnNames = List(i(columnName))
    }
  }

  /**
   * map column, throw exception if value is absent
   */
  def get[T](columnName: String)(implicit format: Format[T]): RowDescriptor[T] = {
    get[T](columnName, { throw new CassandraMapperException(Some("Value required but doesnt exist, column: " + columnName))})
  }

  /**
   * map multiple column to use object field
   */
  def get[T](colums: List[String], default: => T)(implicit format: FormatMultiColumn[T]): RowDescriptor[T] = {
    new RowDescriptor[T] {
      def parse(row: Row) = format.reads(row, columnNames).getOrElse(default)

      def save(query: Insert, value: T) = format.writes(query, columnNames, Some(value))

      val columnNames = colums.map(i(_))
    }
  }
  /**
   * map multiple column to use object field, throw exception if value is absent
   */
  def get[T](columns: List[String])(implicit format: FormatMultiColumn[T]): RowDescriptor[T] = {
    get[T](columns, { throw new CassandraMapperException(message = Some("Value required but doesnt exist, columns: " + columns.mkString(","))) })
  }

  /**
   * map multiple column to use object field, optional
   */
  def getOptional[T](columns: List[String])(implicit format: FormatMultiColumn[T]): RowDescriptor[Option[T]] = {
    new RowDescriptor[Option[T]] {
      def parse(row: Row) = format.reads(row, columnNames)

      def save(query: Insert, value: Option[T]) = format.writes(query, columnNames, value)

      val columnNames = columns.map(i(_))
    }
  }
  
  
  def getRemovableOptional[T](columns: List[String])(implicit format: FormatMultiColumn[T]): RowDescriptor[RemovableOption[T]] = {
    new RowDescriptor[RemovableOption[T]] {
      def parse(row: Row): RemovableOption[T] = format.reads(row, columnNames) match {
        case Some(v) => RSome(v)
        case None => RNone
      }

      def save(query: Insert, value: RemovableOption[T]) = value match {
        case RNone => 
          query
        case RRemoved => 
          columns.foreach { column => query.value(i(column), null)}
          query
        case RSome(v) =>
          format.writes(query, columnNames, value.toOption)
      }

      val columnNames = columns.map(i(_))
    }
  }
  
  /**
   * Execute queries in batch
   */
  def execute(queries: Seq[RegularStatement])(implicit session: Session): QueryResult = queries match {
    case Seq()      => throw new Exception("Impossible")
    case Seq(query) => execute(query)
    case queries    => execute(QueryBuilder.batch(queries.toArray: _*))
  }

  //def withSession[T](f: Session => T)(implicit session: Session) = f(session)

  /**
   * Execute query
   */
  def execute[T](query: Statement)(implicit session: Session): QueryResult = {
    withExceptionProcessing({new QueryResult(session.execute(query), query)}, query)    
  }

  def withExceptionProcessing[T](f: =>T, query: Statement):T =
    try {
      f
    } catch {
      case e: Exception => 
        throw new CassandraMapperException(message = Some(e.getMessage()), cause = Some(e), queryParam = Some(query))
    }

  def generateSetExtractor[T](convertor: String=>T)(row: Row, fieldName: String): Option[Set[T]] = {
      Some(row.getSet(fieldName, classOf[String])).map(_.asScala.map(convertor).toSet)
  }

  def generateSetSaver[T](convertor: T=>String)(query: Insert, field: String, value: Option[Set[T]]) = {
    val simpleSet = value.map(_.map(convertor).asJava)
    simpleSaver(query, field, simpleSet)
  }

}
