package storum.cassandra

import com.datastax.driver.core.ResultSet
import com.datastax.driver.core.Row
import scala.collection.JavaConverters._
import scala.collection.convert.WrapAsScala.asScalaIterator
import com.datastax.driver.core.Statement
import CassandraMapper.withExceptionProcessing


/**
 * Wrapper around cassandra ResultSet, add some useful operations and error handling
 */
class QueryResult(rez: ResultSet, query: Statement) {
  
  private[this] lazy val seqrezVal = rez.all().asScala
  protected def seqrez: Seq[Row] = seqrezVal
    
  def mapFirst[T](f: Row => T): Option[T] = withExceptionProcessing({
    Option(rez.one()).map(f)
  }, query)
    
  def map[T](f: Row => T): Seq[T] = withExceptionProcessing({
    seqrez.map(f).toSeq
  }, query)
  
  def foreach(f: Row => Unit) = withExceptionProcessing({
    rez.iterator().foreach(f)
  }, query)
  
  def slice(from: Int, until: Int): QueryResult = new QueryResult(rez, query) {
    override protected def seqrez: Seq[Row] = super.seqrez.slice(from, until)
    override def foreach(f: Row => Unit) = throw new IllegalArgumentException("QueryResult.foreach can't be applied to sliced result")
  }
  
}
