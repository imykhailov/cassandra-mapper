package storum.cassandra

import com.datastax.driver.core.querybuilder.Insert
import com.datastax.driver.core.Row

/**
 * Convertor for types, that require several columns
 */
trait FormatMultiColumn[T] {
  def writes(query: Insert, columnNames: Seq[String], value: Option[T]): Insert
  def reads(row: Row, columnNames: Seq[String]): Option[T]
}
