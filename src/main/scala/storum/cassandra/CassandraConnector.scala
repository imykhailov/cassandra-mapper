package storum.cassandra

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Session
import com.datastax.driver.core.ProtocolOptions.Compression._
import scala.collection.JavaConverters._

/**
 * Connection provide for using in DI 
 */
trait CassandraConnectionProvider {
  def cluster: Cluster
  def session: Session
}


object CassandraConnectionProvider {
  case class Props(contactPoints: Seq[String], port: Int, keyspace: String)  
}


class PlainCassandraConnectionProvider(  
  contactPoints: Seq[String],
  keyspaceName: String,
  port: Int = -1
) extends CassandraConnectionProvider {
    
    def cassandraConfigName = "default"

    val DefaultCassandrPort = 9042
        
    def loadProps = {
      val prefix = "cassandra." + cassandraConfigName + "."

      val portNum = if (port <0) DefaultCassandrPort else port

      CassandraConnectionProvider.Props(contactPoints, portNum, keyspaceName)
    }

    lazy val cluster = {
      val props = loadProps

      Cluster.builder()
        .addContactPoints(props.contactPoints: _*)
        .withPort(props.port)
        .withCompression(SNAPPY)
        .build()
    }

    lazy val session = {
      cluster.connect(loadProps.keyspace)
    }
}