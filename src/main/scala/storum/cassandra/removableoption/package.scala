package storum.cassandra

package object removableoption {  
  implicit def optionToRemovableOption[T](opt: Option[T]): RemovableOption[T] = RemovableOption(opt) 
}
