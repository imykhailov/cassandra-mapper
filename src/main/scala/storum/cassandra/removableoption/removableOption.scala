package storum.cassandra.removableoption

sealed abstract class RemovableOption[+T] {  
  def toOption: Option[T]
  def get: T
}

case object RNone extends RemovableOption[Nothing] {
  override def toOption: Option[Nothing] = None
  override def get = throw new NoSuchElementException("Ignore.get")
}

case object RRemoved extends RemovableOption[Nothing] {
  override def toOption: Option[Nothing] = None
  override def get = throw new NoSuchElementException("Remove.get")
}

case class RSome[T](newValue: T) extends RemovableOption[T] {
  override def toOption: Option[T] = Some(newValue)
  override def get = newValue
}

object RemovableOption {
  def apply[T](opt: Option[T]): RemovableOption[T] = opt match {
    case None => RNone
    case Some(v) => RSome(v) 
  }
  
}