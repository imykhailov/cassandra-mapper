package storum.cassandra

import com.datastax.driver.core.querybuilder.Insert
import com.datastax.driver.core.Row

/**
 * Convertor for simple types, that require one column in db
 */
trait Format[T] {
  def writes(query: Insert, columnName: String, value: Option[T]): Insert
  def reads(row: Row, columnName: String): Option[T]
}
