package storum.cassandra

import com.datastax.driver.core.querybuilder.Insert
import com.datastax.driver.core.Row
import java.util.UUID
import java.net.InetAddress
import org.joda.time.{DateTime, LocalTime, DateTimeZone}
import scala.collection.JavaConverters._
import java.nio.ByteBuffer
import com.spatial4j.core.shape.Point
import com.spatial4j.core.context.SpatialContext
import scala.concurrent.duration._

trait Formats {

  def formatForEnum[T <: Enumeration](enum: T) =  new Format[T#Value] {
      def reads(row: Row, columnName: String): Option[T#Value] = StringFormat.reads(row, columnName).map(enum.withName(_))
      def writes(query: Insert, columnName: String, value: Option[T#Value]): Insert  = StringFormat.writes(query, columnName, value.map(_.toString))
    }
  
  def simpleSaver[T](query: Insert, field: String, value: Option[T]): Insert =
    value match {
      case Some(realValue) => query.value(field, realValue)
      case None            => query
    }


  implicit val StringFormat = new Format[String] {
    def reads(row: Row, columnName: String): Option[String] = Option(row.getString(columnName))
    def writes(query: Insert, columnName: String, value: Option[String]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val UuidFormat = new Format[UUID] {
    def reads(row: Row, columnName: String): Option[UUID] = Option(row.getUUID(columnName))
    def writes(query: Insert, columnName: String, value: Option[UUID]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val BooleanFormat = new Format[Boolean] {
    def reads(row: Row, columnName: String): Option[Boolean] = Option(row.getBool(columnName))
    def writes(query: Insert, columnName: String, value: Option[Boolean]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val IntFormat = new Format[Int] {
    def reads(row: Row, columnName: String): Option[Int] =
      if (row.isNull(columnName)) {
        None
      } else {
        Some(row.getInt(columnName))
      }
    
    def writes(query: Insert, columnName: String, value: Option[Int]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val LongFormat = new Format[Long] {
    def reads(row: Row, columnName: String): Option[Long] = Option(row.getLong(columnName))
    def writes(query: Insert, columnName: String, value: Option[Long]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val BigDecimalFormat = new Format[BigDecimal] {
    def reads(row: Row, columnName: String): Option[BigDecimal] = Option(row.getDecimal(columnName)).map(BigDecimal(_))
    def writes(query: Insert, columnName: String, value: Option[BigDecimal]): Insert  = simpleSaver(query, columnName, value.map(_.bigDecimal))
  }

  implicit val DoubleFormat = new Format[Double] {
    def reads(row: Row, columnName: String): Option[Double] =
      if (row.isNull(columnName)) {
        None
      } else {
        Some(row.getDouble(columnName))
      }

    def writes(query: Insert, columnName: String, value: Option[Double]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val FloatFormat = new Format[Float] {
    def reads(row: Row, columnName: String): Option[Float] =
      if (row.isNull(columnName)) {
        None
      } else {
        Some(row.getFloat(columnName))
      }

    def writes(query: Insert, columnName: String, value: Option[Float]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val InetAddressFormat = new Format[InetAddress] {
    def reads(row: Row, columnName: String): Option[InetAddress] = Option(row.getInet(columnName))
    def writes(query: Insert, columnName: String, value: Option[InetAddress]): Insert  = simpleSaver(query, columnName, value)
  }

  implicit val JodaDateTimeFormat = new Format[DateTime] {
    def reads(row: Row, columnName: String): Option[DateTime] = Option(row.getDate(columnName)).map(new DateTime(_))
    def writes(query: Insert, columnName: String, value: Option[DateTime]): Insert  = simpleSaver(query, columnName, value map (_.toDate()))
  }
  
  implicit val JodaLocalTimeFormat = new Format[LocalTime] {
    def reads(row: Row, columnName: String): Option[LocalTime] = Option(row.getString(columnName)).map(LocalTime.parse(_))
    def writes(query: Insert, columnName: String, value: Option[LocalTime]): Insert  = simpleSaver(query, columnName, value map (_.toString))
  }
  
  implicit val JodaDateTimeZoneFormat = new Format[DateTimeZone] {
    def reads(row: Row, columnName: String): Option[DateTimeZone] = Option(row.getString(columnName)).map(DateTimeZone.forID(_))
    def writes(query: Insert, columnName: String, value: Option[DateTimeZone]): Insert  = simpleSaver(query, columnName, value map (_.getID))
  }


  implicit val DurationFormat = new Format[Duration] {
    def reads(row: Row, columnName: String): Option[Duration] = Option(row.getLong(columnName)).map(Duration.apply(_, MILLISECONDS))
    def writes(query: Insert, columnName: String, value: Option[Duration]): Insert  = simpleSaver(query, columnName, value.map(_.toMillis))
  }

  implicit val StringMapFormat = new Format[Map[String, String]] {
    def reads(row: Row, columnName: String): Option[Map[String, String]] = {
      val rez = row.getMap(columnName, classOf[String], classOf[java.lang.String]).asScala.map {
        case (key, value) => (key, value)
      }
      if (rez.isEmpty) None else Some(rez.toMap)
    }

    def writes(query: Insert, columnName: String, value: Option[Map[String, String]]): Insert  = {
      val simpleMap = value.map(_.map {
        case (key, value) => (key, value)
      }.asJava)
      simpleSaver(query, columnName, simpleMap)
    }
  }


  implicit val UuidSetFormat = new Format[Set[UUID]] {
    def reads(row: Row, columnName: String): Option[Set[UUID]] = {
      val rez = row.getSet(columnName, classOf[UUID])
      if (rez.isEmpty) None else Some(rez.asScala.toSet)
    }
    def writes(query: Insert, columnName: String, value: Option[Set[UUID]]): Insert  = {
      val simpleSet = value.map(_.asJava)
      simpleSaver(query, columnName, simpleSet)
    }
  }

  implicit val StringSetFormat = new Format[Set[String]] {
    def reads(row: Row, columnName: String): Option[Set[String]] = {
      val rez = row.getSet(columnName, classOf[String])
      if (rez.isEmpty) None else Some(rez.asScala.toSet)
    }
    def writes(query: Insert, columnName: String, value: Option[Set[String]]): Insert  = {
      val simpleSet = value.map(_.asJava)
      simpleSaver(query, columnName, simpleSet)
    }
  }
  
  implicit val BooleanListFormat = new Format[Seq[Boolean]] {
    def reads(row: Row, columnName: String): Option[Seq[Boolean]] = {
      val rez = row.getList(columnName, classOf[java.lang.Boolean])
      if (rez.isEmpty) None else Some(rez.asScala.toSeq.map(scala.Boolean.unbox(_)))
    }
    def writes(query: Insert, columnName: String, value: Option[Seq[Boolean]]): Insert  = {
      val simpleList = value.map(_.asJava)
      simpleSaver(query, columnName, simpleList)
    }
  }


  implicit val ByteArrayFormat = new Format[Array[Byte]] {
    private[this] def byteBufferToArray(bb: ByteBuffer): Array[Byte] = {
      val rez = new Array[Byte](bb.remaining())
      bb.get(rez)
      rez
    }
    def reads(row: Row, columnName: String): Option[Array[Byte]] = Option(row.getBytes(columnName)).map(byteBufferToArray)
    def writes(query: Insert, columnName: String, value: Option[Array[Byte]]): Insert  = simpleSaver(query, columnName, value.map(ByteBuffer.wrap(_)))
  }


  implicit val PointFormat = new FormatMultiColumn[Point] {
    def reads(row: Row, columnNames: Seq[String]): Option[Point] =
      for {
        x <- DoubleFormat.reads(row, columnNames(0))
        y <- DoubleFormat.reads(row, columnNames(1))
      } yield SpatialContext.GEO.makePoint(x, y)
    def writes(query: Insert, columnNames: Seq[String], value: Option[Point]): Insert  =
      value match {
        case Some(point: Point) =>
          query.value(columnNames(0), point.getX())
            .value(columnNames(1), point.getY())
        case None => query
    }
  }

}
