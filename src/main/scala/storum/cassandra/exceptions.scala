package storum.cassandra

import com.datastax.driver.core.Statement
import com.datastax.driver.core.Row


class CassandraMapperException(
  message: Option[String] = None, 
  cause: Option[Throwable] = None,
  queryParam: Option[Statement] = None,
  rowParam: Option[Row] = None    
) extends RuntimeException(message.getOrElse(null), cause.getOrElse(null)) {
  
    val query: Option[Statement] = (queryParam, cause) match {
      case (query: Some[Statement], _) => query
      case (None, Some(e: CassandraMapperException)) => e.query
      case _ => None
    }
    
    val row: Option[Row] = (rowParam, cause) match {
      case (row: Some[Row], _) => row
      case (None, Some(e: CassandraMapperException)) => e.row
      case _ => None
    }
    
    private[this] def trimRowString(row: Row) = {
      val limit = 1024 //1kB
      val str = row.toString
      
      if (str.length() > limit) {
        str.take(limit) + "..."
      } else {
        str
      }
    } 
    
    override def getMessage(): String = { 
      val query = queryParam.map(q => try {
        " \nQuery: " + q.toString()  
      } catch {
        case e: Exception => " \nCan't display query due to: " + e.getMessage()
      })
      
      val row = rowParam.map(r => try {
        "\nRow: " + trimRowString(r)
      } catch {
        case e: Exception => " \nCan't display row due to: " + e.getMessage()
      })
      
      super.getMessage +
      query.getOrElse("") +
      row.getOrElse("")      
    }
}
