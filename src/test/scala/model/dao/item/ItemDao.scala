package model.dao.item

import model.entities.item.Item
import java.util.UUID
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.spatial4j.core.shape.Point
import com.datastax.driver.core.Row
import scala.collection.JavaConverters._
import com.datastax.driver.core.Statement
import model.entities.item.CategoryItemsIndex
import model.utils.IndexUpdater
import com.datastax.driver.core.SimpleStatement
import org.scala_tools.time.Imports.DateTime
import storum.cassandra.CassandraConnectionProvider

trait ItemDao {
  val LOST_ITEMS_CATEGORY = UUID.fromString("47180a6b-c620-4eee-a80e-3321e764405d")
  val DefaultItemsLimit = 100
  
  def save(item: Item): Item
  def findById(itemId: UUID): Option[Item]
  def listByIds(itemIds: Seq[UUID]): Seq[Item]
  def delete(item: Item): Unit
  def findByCategoryId(categoryId: UUID, limit: Int = DefaultItemsLimit, offset: Int = 0): Seq[Item]
  def countByCategoryId(categoryId: UUID): Long
  def unlinkAllItemsFromCategory(categoryId: UUID): Unit
  
  def foreach(f: Item => Unit): Unit
}


class CassandraItemDao (val cassandra: CassandraConnectionProvider) extends ItemDao {
    import storum.cassandra.CassandraMapper._

    implicit def session = cassandra.session

    val ITEM_TABLE = "item"
    val CategoryItemsIndexTableName = "category_items_mindex"

    override def save(item: Item): Item = {
      val itemToSave = item.copy(updateTime = Some(DateTime.now))
      val queries = itemSaver(QueryBuilder.insertInto(ITEM_TABLE), itemToSave) +:
        categoryItemsIndexUpdater.getStatements(findById(itemToSave.itemId), Some(itemToSave))
      execute(queries)
      item
    }

    override def findById(itemId: UUID): Option[Item] = {
      val query = QueryBuilder
        .select(itemDescription.columnNames: _*)
        .from(ITEM_TABLE)
        .where(QueryBuilder.eq(wrapInQuotes("itemId"), itemId))
      execute(query).mapFirst(itemParser)
    }
    
    override def listByIds(itemIds: Seq[UUID]): Seq[Item] = {
      val query = QueryBuilder
        .select(itemDescription.columnNames: _*)
        .from(ITEM_TABLE)
        .where(QueryBuilder.in(wrapInQuotes("itemId"), itemIds: _*))
      
      execute(query).map(itemParser)
    }

    override def findByCategoryId(categoryId: UUID, limit: Int = DefaultItemsLimit, offset: Int = 0): Seq[Item] = {
      val query = QueryBuilder
        .select(wrapInQuotes("itemId"))
        .from(CategoryItemsIndexTableName)
        .where(QueryBuilder.eq(wrapInQuotes("categoryId"), categoryId))
        .limit(offset + limit)
      val itemIds = execute(query).slice(offset, offset + limit).map(_.getUUID("itemId"))
      
      listByIds(itemIds.distinct)
    }

    override def countByCategoryId(categoryId: UUID): Long = {
      val query = new SimpleStatement(s"""SELECT COUNT(*) FROM $CategoryItemsIndexTableName WHERE "categoryId"=${categoryId.toString}""")
      execute(query).mapFirst(_.getLong("count")).get
    }

    override def unlinkAllItemsFromCategory(categoryId: UUID): Unit = {
      findByCategoryId(categoryId).foreach(item=>{
        var itemCategories = item.categories - categoryId
        if (itemCategories.isEmpty) {
          itemCategories += LOST_ITEMS_CATEGORY
        }
        save(item.copy(categories = itemCategories))
      })
    }

    override def delete(item: Item): Unit = {
      val queries = QueryBuilder
        .delete()
        .from(ITEM_TABLE)
        .where(QueryBuilder.eq(wrapInQuotes("itemId"), item.itemId)) +:
        categoryItemsIndexUpdater.getStatements(Some(item), None)
      execute(queries)
    }
    
    /**
     * Use with caution, can be very slow, since it is load all data from items table
     */
    override def foreach(f: Item => Unit): Unit = {
      val query = QueryBuilder
        .select(itemDescription.columnNames: _*)
        .from(ITEM_TABLE)
      
      execute(query).foreach(row => f(itemParser(row)))
    }

    val categoryItemsIndexUpdater = new IndexUpdater {
      type K = UUID //Category
      type V = UUID //Set[Item]
      type I = Item

      def addToIndex(key: K, value: V) =
        categoryItemsIndexSaver(
          QueryBuilder.insertInto(CategoryItemsIndexTableName),
          CategoryItemsIndex(key, value))

      def deleteIndex(key: K, value: V) =
        QueryBuilder.delete().all().from(CategoryItemsIndexTableName)
          .where(QueryBuilder.eq(wrapInQuotes("categoryId"), key))
          .and(QueryBuilder.eq(wrapInQuotes("itemId"), value))

      def extractId(item: I): UUID = item.itemId
      def extractKeys(item: I): Seq[K] = item.categories.toSeq
      def extractValue(item: I): V = item.itemId
    }


    protected val itemDescription =
      get[UUID]("itemId") ~
      getRemovableOptional[Int]("price_amount") ~
      get[String]("name") ~
      get[String]("description", default="") ~
      get[Set[UUID]]("categories", default = Set.empty[UUID]) ~
      get[Map[String,String]]("properties", default = Map[String,String]()) ~
      getRemovableOptional[Point]("location_x" :: "location_y" :: Nil) ~
      getRemovableOptional[String]("mainImage") ~
      get[Set[String]]("images", default = Set.empty[String]) ~
      get[UUID]("userId") ~
      getOptional[DateTime]("createTime") ~
      getOptional[DateTime]("updateTime") ~
      getOptional[String]("externalId")


    protected val itemParser = itemDescription parser {
      case
        itemId ~
        price ~
        name ~
        description ~
        categories ~
        properties ~
        location ~
        mainImage ~
        images ~
        userId ~
        createTime ~
        updateTime ~
        externalId =>
          
        Item(
          itemId = itemId,
          price = price, 
          name = name, 
          description = description,
          categories = categories, 
          properties = properties, 
          location = location, 
          mainImage = mainImage, 
          images = images,
          userId = userId,
          createTime = createTime,
          updateTime = updateTime,
          
          externalId = externalId
        )
    }

    protected val itemSaver = itemDescription saver { (i: Item) =>
      i.itemId ~
      i.price ~
      i.name ~
      i.description ~
      i.categories ~
      i.properties ~
      i.location ~
      i.mainImage ~
      i.images ~
      i.userId ~
      i.createTime ~
      i.updateTime ~ 
      i.externalId
    }

    protected val categoryItemsIndexDesc =
      get[UUID]("categoryId") ~
      get[UUID]("itemId")

    protected val categoryItemsIndexSaver = categoryItemsIndexDesc saver {(i: CategoryItemsIndex) =>
      i.categoryId ~ i.itemId
    }

}

