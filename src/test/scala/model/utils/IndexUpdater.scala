package model.utils

import com.datastax.driver.core.RegularStatement
import java.util.UUID

trait IndexUpdater {

  type K
  type V
  type I

  def addToIndex(key: K, value: V): RegularStatement
  def deleteIndex(key: K, value: V): RegularStatement

  def extractId(item: I): UUID
  def extractValue(item: I): V
  def extractKeys(item: I): Seq[K]

  private[this] def getStatements(value: V, oldSeq: Seq[K], newSeq: Seq[K]): Seq[RegularStatement] = {
   (oldSeq.filterNot(newSeq.contains).map(deleteIndex(_, value)) ++
      newSeq.filterNot(oldSeq.contains).map(addToIndex(_, value))).toList
  }

  def getStatements(oldItem: Option[I], newItem: Option[I]): Seq[RegularStatement] = {
     (oldItem, newItem) match {
        case (Some(oldItem), Some(newItem)) =>
          if (extractId(oldItem) == extractId(newItem)) {
            getStatements(extractValue(newItem), extractKeys(oldItem), extractKeys(newItem))
          } else {
            throw new IllegalArgumentException(s"It is different items, since itemId are distinct, ${extractId(oldItem)} != ${extractId(newItem)}")
          }
        case (None, Some(newItem)) => getStatements(extractValue(newItem), Nil, extractKeys(newItem))
        case (Some(oldItem), None) => getStatements(extractValue(oldItem), extractKeys(oldItem), Nil)
        case (None, None) => Nil
      }
  }
}
