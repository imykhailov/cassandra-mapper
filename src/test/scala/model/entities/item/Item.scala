package model.entities.item

import java.util.UUID
import com.spatial4j.core.shape.Point
import org.scala_tools.time.Imports.DateTime
import java.net.URL
import storum.cassandra.removableoption._


case class Item(
  itemId: UUID,
  price: RemovableOption[Int] = RNone,
  name: String,
  description: String,
  categories: Set[UUID],
  properties: Map[String, String] = Map(),
  location: RemovableOption[Point] = None,
  mainImage: RemovableOption[String] = RNone,
  images: Set[String] = Set.empty,
  userId: UUID,
  
  createTime: Option[DateTime] = None,
  updateTime: Option[DateTime] = None,

  externalId: Option[String] = None
)

case class CategoryItemsIndex(
  categoryId: UUID,
  itemId:     UUID
)

case class RichItemImageInfo(
  imageId: String,
  imageUrl: URL
)

case class RichItemCategoryInfo(
  categoryId: UUID,
  categoryName: Option[String]
)

case class ItemModificator (
  price: RemovableOption[Int] = RNone,
  name: Option[String] = None,
  description: Option[String] = None,
  categories: Option[Set[UUID]] = None,
  properties: Option[Map[String, String]] = None,
  location: RemovableOption[Point] = RNone,
  mainImage: RemovableOption[String] = RNone
) {
    
  def updateItem(item: Item) = {    
    var rez = item
    
    rez = rez.copy(price = price)
    
    name.foreach { v =>
      rez = rez.copy(name = v)  
    }
    
    description.foreach { v =>
      rez = rez.copy(description = v)  
    }
    
    categories.foreach { v =>
      rez = rez.copy(categories = v)  
    }
    
    properties.foreach { v =>
      rez = rez.copy(properties = v)  
    }
    
    rez = rez.copy(location = location)
    
    rez = rez.copy(mainImage = mainImage) 
      
    rez
  }
  
  def createNewItem(itemId: UUID, userId: UUID): Item = {
    def fieldIsEmptyError(field: String) = throw new Exception(s"Field '' is empty")
    
    if (name.isEmpty) {
      fieldIsEmptyError("name")
    } else if (description.isEmpty) {
      fieldIsEmptyError("description")
    } else if (categories.isEmpty) {
      fieldIsEmptyError("categories")
    } else {
      Item(
        itemId = itemId,
        price = price,
        name = name.get,
        description = description.get,
        categories = categories.get,
        properties = properties.getOrElse(Map.empty),
        location = location,
        mainImage = mainImage,
        userId = userId
      )
    }
  }
}

