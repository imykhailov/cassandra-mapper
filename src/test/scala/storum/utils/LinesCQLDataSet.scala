package storum.utils

import org.cassandraunit.dataset.cql.AbstractCQLDataSet
import java.io.InputStream
import java.io.ByteArrayInputStream

class LinesCQLDataSet(lines: Seq[String], createKeyspace: Boolean = true, deleteKeyspace: Boolean = true, keyspaceName: String = null) 
  extends AbstractCQLDataSet("", createKeyspace, deleteKeyspace, keyspaceName) {

  protected override def getInputDataSetLocation(dataSetLocation: String): InputStream = {
    val bytes = lines.mkString("\n").getBytes()
    new ByteArrayInputStream(bytes)
  }
}