package storum.utils

import org.apache.cassandra.config.DatabaseDescriptor
import org.cassandraunit.CQLDataLoader
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet
import org.cassandraunit.utils.EmbeddedCassandraServerHelper
import org.specs2.mutable
import org.specs2.specification.Fragments
import org.specs2.specification.Step
import com.datastax.driver.core.Cluster
import com.datastax.driver.core.ProtocolOptions.Compression._
import org.apache.cassandra.config.Config
import org.cassandraunit.dataset.CQLDataSet
import com.datastax.driver.core.querybuilder.QueryBuilder
import java.util.Date
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.Files
import java.nio.file.attribute.FileTime
import com.datastax.driver.core.Session
import scala.collection.JavaConverters._
import scala.util.Try
import scala.util.control.NonFatal
import java.nio.file.Path
import scala.io.Source
import storum.cassandra.CassandraConnectionProvider.Props
import storum.cassandra.PlainCassandraConnectionProvider
import scala.util.Success

/**
 * Warning. This class s very overoptimized. So, you can't use different cassandra connection across different unit tests
 * 
 */
trait CassandraUsing extends mutable.Specification {
  lazy val cassandraInstance = {
    //val cassandraInstance = if (CassandraStandalone.isAccessible) CassandraStandalone else CassandraEmbeded
    val cassandraInstance = CassandraEmbeded
    cassandraInstance.startDb
    cassandraInstance.initDb
    cassandraInstance
  }
  
  def connectionProvider = new PlainCassandraConnectionProvider(
    Seq("localhost"), cassandraInstance.keyspaceName, cassandraInstance.port
  )
}

trait CassandraInstance {
  def keyspaceName: String
  def port: Int
  def startDb(): Unit = {}
  def initDb(): Unit = {}
    
  val REUSE_DB_JAVA_PROPERTY = "storum.test.reusedb"
    
  val SCHEMA_CQL_FILE = Paths.get("testResources/cql/schema.cql")
  val DATA_CQL_FILE = Paths.get("testResources/cql/data.cql")
    
  protected def parseProps(config: Map[String, Any]): Props =  
    new PlainCassandraConnectionProvider(
      Seq("localhost"),
      keyspaceName,
      port
    ).loadProps
    
  protected def loadCql(filepath: Path, recreateKeyspace: Boolean = true): CQLDataSet = {
    val lines = Source.fromFile(filepath.toAbsolutePath().toString()).getLines().toSeq
    
    val safeLines = lines
      .map { line =>
        val commentStartAt = (line.indexOf("--"), line.indexOf("//")) match {
          case (index, -1) => index
          case (-1, index) => index
          case (index1, index2) => Math.min(index1, index2) 
        }
        if (commentStartAt >= 0) {
          line.take(commentStartAt)  
        } else {
          line
        }
      }
      .filter(_.length() > 0)
        
    val dataset = if (recreateKeyspace) {
      new LinesCQLDataSet(safeLines, keyspaceName = keyspaceName)
    } else {
      new LinesCQLDataSet(safeLines, false, false, keyspaceName)
    }
    //if (dataset.getCQLStatements().isEmpty()) throw new Exception(s"${filepath.toAbsolutePath()} is not found or empty");
    dataset
  }
    
  protected def defaultInitDb(props: Props) =  {
    val schema = loadCql(SCHEMA_CQL_FILE)
    val data = loadCql(DATA_CQL_FILE, recreateKeyspace = false)
    
    val dataloader = new CQLDataLoader(props.contactPoints.head, props.port) 
    val session = dataloader.getSession()
    
    if (scala.sys.props.get(REUSE_DB_JAVA_PROPERTY) != Some("true")) {
      dbSchemaCreationTime(session) match {
        case None => 
          recreateDb(props, Seq(schema, data))
        case Some(timeOfCreation) if timeOfCreation.getTime() < schemaFileModificationTime.toMillis() =>
          recreateDb(props, Seq(schema, data))
        case _ => {
          truncateAllTables(session)
          dataloader.load(data)
        }
      } 
    }  
  }
  
  protected def truncateAllTables(session: Session): Unit = {
    println("Reuse existed DB. Reload data.")
    session.execute(s"SELECT columnfamily_name FROM system.schema_columnfamilies  WHERE keyspace_name = '$keyspaceName';")
    .all().asScala
    .map(_.getString("columnfamily_name"))
    .foreach( tablename => 
      session.execute(s"""TRUNCATE "$tablename"""")  
    )
  } 
  
  protected val META_TABLE_NAME = "\"storumCassandraTesting_creationDate\""
 
  protected def schemaFileModificationTime: FileTime = {
    Files.readAttributes(SCHEMA_CQL_FILE, classOf[BasicFileAttributes])
      .lastModifiedTime()
  }
  
  protected def dbSchemaCreationTime(session: Session): Option[Date] = {    
    
    try {
      session.execute(s"""USE "${keyspaceName}"""")
      val query = QueryBuilder
        .select().all()
        .from(META_TABLE_NAME)
        .where(QueryBuilder.eq("dummykey", "dummykey"))
      
      session.execute(query).all().asScala.toSeq match {
        case Seq() => None
        case Seq(row) => Some(row.getDate("date_of_creation"))
      }
    } catch {
      case NonFatal(e) => None
    }
  }
    
  protected def recreateDb(props: Props, dataSets: Seq[CQLDataSet]) = {
    println("Recreate DB from *.cql files")
    val dataLoader = new CQLDataLoader(props.contactPoints.head, props.port);
    
    dataSets.foreach(dataLoader.load)
    
    val session = dataLoader.getSession()
    session.execute(s"""CREATE TABLE ${META_TABLE_NAME} (dummykey text PRIMARY KEY, date_of_creation timestamp) """)
    val query = QueryBuilder
      .insertInto(keyspaceName, META_TABLE_NAME)
      .value("dummykey", "dummykey")
      .value("date_of_creation", new Date(schemaFileModificationTime.toMillis()))
    session.execute(query)
     
  }
    
}

/*
object CassandraStandalone extends CassandraInstance {
  val port = 9042
  val keyspaceName = "cassandra_test_db"
    
  lazy val props = parseProps(playConfig)
    
  lazy val cluster = 
    Cluster.builder()  
      .addContactPoints(props.contactPoints: _*)
      .withPort(props.port)
      .withCompression(SNAPPY)
      .build()
  

  lazy val isAccessible = 
    try {
      cluster.connect()
      true
    } catch {
      case e: Throwable => {println(e.toString); false}
    }

  override def startDb() = {
    println("Using CassandraStandalone")
  }

  lazy val initedDb =  defaultInitDb(props)
  override def initDb() = initedDb

}*/


object CassandraEmbeded extends CassandraInstance {
  val keyspaceName = "cassandra_test_db"

  lazy val port = {
    19042
    //startDb()
    //DatabaseDescriptor.getNativeTransportPort()
  }

  val startedDb = {
    println("Using CassandraEmbeded")
    
    val connectionProvider = new PlainCassandraConnectionProvider(
      Seq("localhost"), keyspaceName, 19042
    )
    val needToStart = Try({connectionProvider.cluster.connect()}).isFailure
    
    if (needToStart) {
      try {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra("cassandra2.yaml");
      } catch {
        case e: Throwable => 
          println(e)
          e.printStackTrace()
          throw e
      }
    }
  }
  override def startDb() = {
    startedDb
  }

  lazy val initedDb = 
    defaultInitDb(Props(
      contactPoints= Array("localhost"),
      port = port,
      keyspace = keyspaceName
    ))  
 
 
  override def initDb() = initedDb

}