package test

import org.specs2.mutable
import java.util.UUID
import model.entities.item._
import org.scala_tools.time.Imports.DateTime
import storum.cassandra.removableoption._
import com.spatial4j.core.context.SpatialContext
import model.dao.item.ItemDao
import model.dao.item.CassandraItemDao
import storum.utils.CassandraUsing

class ItemDaoTest 
  extends mutable.SpecificationWithJUnit 
  with CassandraUsing {
  
  import model._
    
  val AdminUserUuid = UUID.randomUUID()
    
  val dao = new CassandraItemDao(connectionProvider)
  
  "Item" should {
    "contain createand update dates" in {
      val item = Item(UUID.randomUUID(),
        name = "item1",
        description = "item1description",
        categories = Set(UUID.randomUUID()),
        userId = AdminUserUuid,
        createTime = Some(DateTime.now))
        
      dao.save(item)
      val rez = dao.findById(item.itemId) 
      rez must beSome
      rez.get.createTime must beSome
      rez.get.updateTime must beSome
    }
    
    "be created and found by id" in {
      val item = Item(UUID.randomUUID(),
        name = "item1",
        description = "item1description",
        categories = Set(UUID.randomUUID()),
        userId = AdminUserUuid,
        mainImage = Some(UUID.randomUUID().toString + ".jpeg"),
        images = Set(UUID.randomUUID().toString + ".jpeg"))        
     
      dao.save(item)      
      dao.findById(item.itemId).map(clearCreateUpdateTime) must beSome(item)
    }
    
    "be found by category" in {
      val categoryId = UUID.randomUUID()
      val item = Item(UUID.randomUUID(),
        name = "item1",
        userId = AdminUserUuid,
        description = "item1description",
        categories = Set(categoryId))
     
      dao.save(item)      
      dao.findByCategoryId(categoryId).map(clearCreateUpdateTime) must contain(item) 
    }
    
  }
  
  "Field removing" should {
    "work for simple fields" in {
      val categoryId = UUID.randomUUID()
      val item = Item(UUID.randomUUID(),
        name = "item1",
        userId = AdminUserUuid,
        description = "item1description",
        categories = Set(categoryId),
        mainImage = RSome("mmmmmm")
      )
      
      dao.save(item)
      
      dao.findById(item.itemId).get.mainImage.toOption must beSome("mmmmmm")
      
      dao.save(item.copy(
        mainImage = RRemoved    
      ))
      
      dao.findById(item.itemId).get.mainImage.toOption must beNone
    }
    
    "work for complex fields" in {
      val categoryId = UUID.randomUUID()
      val point = SpatialContext.GEO.makePoint(10, 15)
      val item = Item(UUID.randomUUID(),
        name = "item1",
        userId = AdminUserUuid,
        description = "item1description",
        categories = Set(categoryId),
        location = RSome(point)
      )
     
      dao.save(item)
      
      dao.findById(item.itemId).get.location.toOption must beSome(point)
      
      dao.save(item.copy(
        location = RRemoved    
      ))
      println(item.itemId)
      dao.findById(item.itemId).get.mainImage.toOption must beNone
    }
  }
  
  def clearCreateUpdateTime(item: Item) = item.copy(
    createTime = None,
    updateTime = None
  )
}